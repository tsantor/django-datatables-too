## 0.1.1 2018‑09‑25
- Initial release

## 0.1.2 2019‑08‑16
- **Fixed** bug when DataTable was set to `ordering: false` in JavaScript

## 0.1.3 2019‑08‑16
- **Fixed** fixed issue where HISTORY.rst was not included in the package causing it to fail upon install

## 0.1.4 2021-08-30
- **Fixed** sorting now actually works

## 0.1.5 2023-01-13
- **Added** `Cache-Control: no-cache` header
